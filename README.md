```
 _____         ____  _____    ___ ___ ___ ___ 
|  _  |___ ___|    \| __  |  |_  |   |   |   |
|   __|  _| -_|  |  | __ -|  |  _| | | | | | |
|__|  |_| |___|____/|_____|  |___|___|___|___|
                                              
```
                                      
PreDB 2000 is a project inspired by [Shodan 2000](https://2000.shodan.io). It pulls data from [PreeDB](https://predb.net/) and displays the latest scene release.

This project was built using [SvelteKit]([SvelteKit](https://kit.svelte.dev/)), [Tailwind CSS](https://tailwindcss.com/), and [ThreeJS ](https://threejs.org/).

Credits:
- Background: [Sea Of Stars by Jeremy Perkins](https://unsplash.com/photos/uhjiu8FjnsQ)
- Music: [Street Dancing by Timecrawler 82](https://freemusicarchive.org/music/Timecrawler_82/Osaka_Lights/Street_Dancing)

![Demo screenshot 1](images/screenshot1.png)
![Demo screenshot 2](images/screenshot2.png)
![Demo screenshot 3](images/screenshot3.png)
