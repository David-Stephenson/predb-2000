export function unixToHumanReadable(unixTimestamp) {
  const date = new Date(unixTimestamp * 1000);
  const formattedDate = date.toLocaleString();

  return formattedDate;
}
